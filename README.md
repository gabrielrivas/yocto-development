#Build container
docker build -t yocto-development:<version> ubuntu<version>

#Setup bitbake environment
DISTRO=fsl-imx-wayland  MACHINE=imx8mqevk source fsl-setup-release.sh -b build_imx

#Run image
docker run -itd --net=bridge -p 22:22 -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /home/linux/.ssh:/home/linux/.ssh -v /home/linux/transfer:/home/linux/transfer -v /opt/downloads:/opt/downloads --name imx-builder -h imx-builder yocto-development:latest bash -c "/usr/sbin/sshd -D"

#Run image passing a device file
docker run -itd --net=bridge -p 22:22 -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /home/linux/.ssh:/home/linux/.ssh -v /home/linux/transfer:/home/linux/transfer -v /opt/downloads:/opt/downloads --device=/dev/ttyUSB0:/dev/ttyUSB0 --name tel52 -h tel52 buildsys-ubuntu-16:latest  bash -c "/usr/sbin/sshd -D"

#Run image with rebar3 configuration
docker run -itd --net=bridge -p 22:22 -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v /home/linux/.ssh:/home/linux/.ssh -v /home/linux/transfer:/home/linux/transfer -v /opt/downloads:/opt/downloads -v /home/linux/.cache/rebar3/hex:/home/linux/.cache/rebar3/hex --name imx-builder -h imx-builder yocto-development:16 bash -c "/usr/sbin/sshd -D"

#Connect from client
ssh -X linux@localhost
